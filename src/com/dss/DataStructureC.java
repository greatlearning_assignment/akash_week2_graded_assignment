package com.dss;

import java.util.ArrayList;
import java.util.TreeMap;

public class DataStructureC {

	public void monthlySalary(ArrayList<Employee> employees)
	{
		try 
		{
			TreeMap<Integer,Float> monthlySalary_id=new TreeMap<>();
			for(Employee emp1:employees)
			{
				monthlySalary_id.put(emp1.getId(),(float)Math.floor((float)emp1.getSalary()/12));
			}
			System.out.println("Monthly Salary of employee along with their Id is:");
			System.out.println(monthlySalary_id);
		}
		catch(Exception exception)
		{
			System.out.println("exception found"+exception.getMessage());
		}
	}
}
