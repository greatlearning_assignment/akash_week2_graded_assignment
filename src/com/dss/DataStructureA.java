package com.dss;

import java.util.ArrayList;
import java.util.Collections;

public class DataStructureA {

	public void sortingNames(ArrayList<Employee> employees) {
		ArrayList<String> names=new ArrayList<String>();
		for(Employee e:employees)
		{
			names.add(e.getName());
		}
		Collections.sort(names);
		System.out.println("Name of all the employees in sorted order are: ");
		System.out.println(names);
		System.out.println();
	}
}
