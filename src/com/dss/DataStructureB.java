package com.dss;

import java.util.ArrayList;

import java.util.TreeMap;

public class DataStructureB {

	public void cityNameCount(ArrayList<Employee> employees){
		TreeMap<String,Integer> cityCount= new TreeMap<>();
		for(Employee emp:employees) 
		{
			String city=emp.getCity();
			if(cityCount.containsKey(city)) 
			{
				cityCount.replace(city, (((int)cityCount.get(city))+1));
			}
			else 
			{
				cityCount.put(city, 1);
			}
		}
		System.out.println("Count of Employees from each city: ");
		System.out.println(""+cityCount);
		System.out.println();
	}
}

