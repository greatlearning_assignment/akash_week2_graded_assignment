package com.dss;

public class Employee {

	int id;

	String name;

	int age;

	int salary; // per annum

	String department;

	String city;

	//setters are used to set values for variables
	public void setId(int id)
	{
		if(id<0) {
			throw new IllegalArgumentException("id is not valid ");
		}
		this.id=id;
	}
	public void setName(String name)
	{
		if(name==null|| name.isEmpty())
		{
			throw new IllegalArgumentException("name should not be empty");

		}
		this.name=name;
	}
	public void setAge(int age)
	{
		if(age<0)
		{
			throw new IllegalArgumentException("age cannot be 0");

		}
		this.age=age;
	}
	public void setSalary(int salary)
	{
		if(salary<0)
		{
			throw new IllegalArgumentException("salary cannot be zero");

		}
		this.salary=salary;
	}
	public void setDepartment(String department)
	{
		if(department==null || department.isEmpty())
		{
			throw new IllegalArgumentException("department name should not be empty");

		}
		this.department=department;
	}
	public void setCity(String city)
	{
		if(city==null || city.isEmpty())
		{
			throw new IllegalArgumentException("city name should not be empty");

		}
		this.city=city;
	}
	//getters are used to retrieve the stored values
	public int getId() {
		return id;
	}
	public String getName()
	{
		return name;
	}
	public int getAge()
	{
		return age;
	}
	public int getSalary()
	{
		return salary;
	}
	public String getDepartment()
	{
		return department;
	}

	public String getCity() 
	{
		return city;
	}
} 




