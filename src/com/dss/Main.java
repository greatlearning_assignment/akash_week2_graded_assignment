package com.dss;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Employee> emp = new ArrayList<>(); 
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		Employee e4 = new Employee();
		Employee e5 = new Employee();
		// setting details of emp1 
		e1.setId(1);
		e1.setName(" Aman  ");
		e1.setAge(20);
		e1.setSalary(1100000);
		e1.setDepartment(" IT");
		e1.setCity("Delhi");
		// setting details of emp2 
		e2.setId(2);
		e2.setName(" Bobby ");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("  Hr");
		e2.setCity("Bombay");
		// setting details of emp3 
		e3.setId(3);
		e3.setName(" Zoe   ");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin");
		e3.setCity("Delhi");
		// setting details of emp4 
		e4.setId(4);
		e4.setName(" Smitha");
		e4.setAge(21);
		e4.setSalary(1000000 );
		e4.setDepartment(" IT");
		e4.setCity("Chennai");
		// setting details of emp5 
		e5.setId(5);
		e5.setName(" Smitha");
		e5.setAge(24);
		e5.setSalary(12000000);
		e5.setDepartment("Hr");
		e5.setCity("Bengaluru");

		//Adding Details
		emp.add(e1);
		emp.add(e2);
		emp.add(e3);
		emp.add(e4);
		emp.add(e5);
		System.out.println("List of Employees: ");
		System.out.println("S.NO."+" "+"Name"+"   "+"Age"+" "+"Salary(INR)"+" "+"Department"+"  "+"Location");
		for(Employee emp1: emp) {
			System.out.println(" "+emp1.getId()+"   "+emp1.getName()+" "+emp1.getAge()+
					"   "+emp1.getSalary()+"      "+emp1.getDepartment()+"        "+emp1.getCity());
		}
		System.out.println();
		DataStructureA ob1 = new DataStructureA();
		ob1.sortingNames(emp);
		DataStructureB ob2 = new DataStructureB();
		ob2.cityNameCount(emp);
		DataStructureC ob3= new DataStructureC();
		ob3.monthlySalary(emp);
	}

} 


